//
//  EnforcementConstants.swift
//  Enforcement
//
//  Created by PAVANI VEMPATI on 12/07/17.
//  Copyright © 2017 Viswa Gopisetty. All rights reserved.
//

import Foundation

enum ScannedCodeType {
    case RetailerInfo
    case BottleVerification
    case CaseVerification
    case TransportPermitVerification
    case ExportPermitVerification
    case ImportPermitVerification
    case EventpermitVerification
    case InterDepotTransferVerification
    case TP_ID_CS_BTPVerification
}

class EnforcementCommonConstants {
    static let STATUS : String = "status"
    
    static let DEFAULT_STATE : String = "DEFAULT_STATE"
    static let DEFAULT_LOCATION : String = "DEFAULT_LOCATION"
    static let DEFAULT_CORDINATE : String = "DEFAULT_CORDINATE"
    static let DEFAULT_CREDENTIALS : String = "DEFAULT_CREDENTIALS"
    static let USER_NAME : String = "USER_NAME"
    static let PASSWORD : String = "PASSWORD"
    
    static let LOGIN_SUCCESS : String = "LOGIN_SUCCESS"
}

class UserAuthentication {
    static let NAME : String = "name"
    static let STATE : String = "state"
    static let DISPLAY_NAME : String = "displayName"
    static let VERSION : String = "version"
    static let USER : String = "user"
    static let STATUS : String = "status"
    static let EMAIL_ID : String = "emailId"
    static let ROLE_ID : String = "roleId"
    static let PWD : String = "pwd"
    static let LOC_TYPE : String = "locType"
    static let MOBILE_NUMBER : String = "mobileNumber"
    static let LOCATION_NAME : String = "locationName"
    static let LOCATION_ID : String = "locationId"
}

class RetailerInfo {
    static let STATUS : String = "status"
    static let VENDOR_NAME : String = "vendorName"
    static let VENDOR_CODE : String = "vendorCode"
    static let REGION : String = "region"
    static let EX_DIST_CODE : String = "exDistCode"
    static let REV_DIST_CODE : String = "revDistCode"
    static let APBDL_DEPOT_CODE : String = "apbclDepotCode"
    static let RETAILER_CODE : String = "retailerCode"
    static let GAZETT_CODE : String = "gazetteCode"
    static let EX_CIRCLE_CODE : String = "exCircleCode"
    static let VENDOR_TYPE_CODE : String = "vendorTypeCode"
    static let VENDOR_REG_NO : String = "vendorRegNo"
    static let LICENSE_NO : String = "licenseNo"
    static let LICENSE_NAME : String = "licenseeName"
    static let LICENSE_ISSUE_DATE : String = "licenseIssueDate"
    static let LIENSE_EXPIRY_DATE : String = "licenseExpiryDate"
    static let PAN_NUMBER : String = "panNumber"
    static let VENDOR_ID : String = "venderId"
}

class BottleVerification {
    static let STATUS : String = "status"
    static let VALID : String = "valid"
    static let PRODUCT_NAME : String = "productName"
    static let SERIES : String = "series"
    static let UPDATED_ON : String = "updatedOn"
    static let PRODUCT_CODE : String = "productCode"
    static let SIZE_CODE : String = "sizeCode"
    static let CASE_CODE : String = "caseCode"
    static let BATCH_NUMBER : String = "batchNumber"
    static let MANUFACTURING_DATE : String = "manufacturingDate"
    static let MANUFACTURING_LOC_NAME : String = "manufacturinLocationName"
    static let DEPO_LOCATION_NAME : String = "depoLocationName"
    static let VENDOR_LOCATION_NAME : String = "vendorLocationName"
    static let CURRENT_LOC_ID : String = "currentLocationId"
    static let BRAND_NUMBER : String = "brandNumber"
    static let PURPOSE : String = "purpose"
    static let MRP : String = "mrp"
    static let EX_DIST_CODE : String = "exDistCode"
    static let REV_DIST_CODE : String = "revDistCode"
    static let APBCL_DEPOT_CODE : String = "apbclDepotCode"
    static let RETAILER_CODE : String = "retailerCode"
    static let GAZETTE_CODE : String = "gazetteCode"
    static let EX_CIRCLE_CODE : String = "exCircleCode"
    static let SHCODE : String = "shcode"
    static let PACK_TYPE : String = "packType"
    static let MFG_CODE : String = "mfg_code"
}

class CaseVerification {
    static let STATUS : String = "status"
    static let PRODUCT_NAME : String = "productName"
    static let PRODUCT_CODE : String = "productCode"
    static let SIZE_CODE : String = "sizeCode"
    static let CASE_CODE : String = "caseCode"
    static let CASE_TYPE : String = "caseType"
    static let BATCH_NUMBER : String = "batchNumber"
    static let LOT_NUMBER : String = "lotNumber"
    static let MANUFACTURING_DATE : String = "manufacturingDate"
    static let MANUFACTURING_LOC_NAME : String = "manufacturinLocationName"
    static let DEPO_LOCATION_NAME : String = "depoLocationName"
    static let VENDOR_LOCATION_NAME : String = "vendorLocationName"
    static let CURRENT_LOC_ID : String = "currentLocationId"
    static let UNITS_PER_CASE : String = "unitsPerCase"
    static let BRAND_NUMBER : String = "brandNumber"
    static let PURPOSE : String = "purpose"
    static let MRP : String = "mrp"
    static let DIST_SYNC_DATE : String = "distSyncDate"
    static let SHIPMENT_DATE : String = "shipmentDate"
    static let EX_DIST_CODE : String = "exDistCode"
    static let REV_DIST_CODE : String = "revDistCode"
    static let APBCL_DEPOT_CODE : String = "apbclDepotCode"
    static let RETAILER_CODE : String = "retailerCode"
    static let GAZETTE_CODE : String = "gazetteCode"
    static let EX_CIRCLE_CODE : String = "exCircleCode"
}

class TransportThroughPermit {
    static let ID : String = "id"
    static let STATUS : String = "status"
    static let CREATED_BY : String = "createdBy"
    static let CREATED_ON : String = "createdOn"
    static let ROUTE : String = "route"
    static let IMPORT_PERMIT_NO : String = "importPermitNo"
    static let IMPORT_PERMIT_DATE : String = "importPermitDate"
    static let IMPORT_PERMIT_VALID_FROM : String = "importPermitValidFrom"
    static let IMPORT_PERMIT_VALID_TO : String = "importPermitValidTo"
    static let IMPORT_PERMIT_ISSUED_BY : String = "importPermitIssuedBy"
    static let CONSIGNMENT_ORIGIN : String = "consignmentOrigin"
    static let EXPORT_PERMIT_NO : String = "exportPermitNo"
    static let EXPORT_PERMIT_DATE : String = "exportPermitDate"
    static let EXPORT_PERMIT_ISSUED_BY : String = "exportPermitIssuedBy"
    static let CONSIGNMENT_DESTINATION : String = "consignmentDestination"
    static let IMPORTER_NAME : String = "importerName"
    static let EXPORTER_NAME : String = "exporterName"
    static let CONVEYANCE_MODE : String = "conveyanceMode"
    static let OLD_VEHICLE_NO : String = "oldVehicleNo"
    static let THRU_PERMIT_VALID_FROM : String = "thruPermitValidFrom"
    static let THRU_PERMIT_VALID_TO : String = "thruPermitValidTo"
    static let START_CHECK_POINT : String = "startCheckPoint"
    static let END_CHECK_POINT : String = "endCheckPoint"
    static let THROUGH_PERMIT_NO : String = "throughPermitNo"
    static let TRANSPORTER_NAME : String = "transporterName"
    static let TRANSPORTER_ADDRESS : String = "transporterAddress"
    static let NEW_VEHICLE_NO : String = "newVehicleNo"
    static let EXTEN_VAL_TO : String = "extenValTo"
    static let REVAL_REQ_BY : String = "revalReqBy"
    static let INTOXICANT_TYPE : String = "intoxicantType"
    static let INTOXICANT_BRAND : String = "intoxicantBrand"
    static let INTOXICANT_ML : String = "intoxicantMl"
    static let INTOXICANT_STRENGTH : String = "intoxicantStrength"
    static let INTOXICANT_NO_CASES : String = "intoxicantNoCases"
    static let INTOXICANT_BL : String = "intoxicantBl"
    static let INTOXICANT_PPL : String = "intoxicantPpl"
}

class ExportPermitVerification {
    static let STATE : String = "state"
    static let STATUS : String = "status"
    static let SOURCE_NAME : String = "sourceName"
    static let PRODUCT_NAME : String = "productName"
    static let CREATED_BY : String = "createdBy"
    static let CREATED_ON : String = "createdOn"
    static let TP_NUMBER : String = "tpNumber"
    static let LR_NUMBER : String = "lrNumber"
    static let VEHICLE_NUMBER : String = "vehiclNumber"
    static let ISSUE_DATE : String = "issueDate"
    static let ISSUE_TIME : String = "issueTime"
    static let VALID_DATE : String = "validDate"
    static let VALID_TIME : String = "validTime"
    static let ROUTE : String = "route"
    static let SHIPMENT_ID : String = "shipmetId"
    static let LR_DATE : String = "lrDate"
    static let TRANSPORTER : String = "transporter"
    static let VALID_PERIOD : String = "validPeriod"
    static let DEST_NAME : String = "destName"
    static let INTIATOR_ID : String = "intiatorId"
    static let APPROVAL_DATE : String = "approvalDate"
    static let DEPARTURE_DATE : String = "departureDate"
    static let DEPARTURE_TIME : String = "departureTime"
    static let INVOICE_PROFORMA_ID : String = "invoiceProformaId"
    static let PRODUCT_CODE : String = "productCode"
    static let SIZE_CODE : String = "sizeCode"
    static let SHIPMENT_QTY : String = "shipmentQty"
    static let DC_NUMBER : String = "dcNumber"
    static let OUTLET_REG_CODE : String = "outletRegCode"
    static let DEPOT_NAME : String = "depotName"
    static let OUTLET_NAME : String = "outletName"
    static let AMT_TOTAL : String = "amtTotal"
}

class EventPermitVerification {
    static let STATUS : String = "status"
    static let ISSUE_DATE : String = "issueDate"
    static let PERMIT_NUMBER : String = "permitNumber"
    static let ISSUE_BY : String = "issuedBy"
    static let PERMIT_FEE : String = "permitFee"
    static let PAYMENT_MODE : String = "paymentMode"
    static let INSTRUMENT_NUMBER : String = "instrumentNumber"
    static let BANK : String = "bank"
    static let INSTRUMENT_DATE : String = "instrumentDated"
    static let EVENT_DATE : String = "eventDate"
    static let EVENT_ADDRESS : String = "eventAddress"
    static let BOUNDARY_EAST : String = "boundaryEast"
    static let BOUNDARY_WEST : String = "boundaryWest"
    static let BOUNDARY_NORTH : String = "boundaryNorth"
    static let BOUNDARY_SOUTH : String = "boundarySouth"
}

class ImportPermitVerification {
    static let STATUS : String = "status"
    static let VALIDITY_PERIOD : String = "validityPeriod"
    static let ROUTE : String = "route"
    static let PRODUCT_CODE : String = "productCode"
    static let SIZE_CODE : String = "sizeCode"
    static let DEPOT_NAME : String = "depotName"
    static let BANK : String = "bank"
    static let PERMIT_NO : String = "permitNo"
    static let PERMIT_DATE : String = "permitDate"
    static let PO_NUMBER : String = "ponumber"
    static let PO_DATE : String = "podate"
    static let LICENSE_ID : String = "licenseeId"
    static let INTOXICANT_EXPORTER_LOC : String = "intoxicantExporterLoc"
    static let INTOXICANT_EXPORTER_PLACE : String = "intoxicantExporterPlace"
    static let INPORT_PURPOSE : String = "importPurpose"
    static let TP_BANK_CODE : String = "tpBarcode"
    static let IMPORT_ID : String = "importId"
    static let INTOXICANT_EXPORTER_NAME : String = "intoxicantExporterName"
    static let BRAND_NAME : String = "brandName"
    static let SIZE_IN_ML : String = "sizeInMl"
    static let QTY_IN_CASE : String = "qtyInCases"
    static let PAYMENT_HEAD : String = "paymentHead"
    static let CHALLAN_NO : String = "challanNo"
    static let CHALLAN_AMT : String = "challanAmt"
    static let CHALLAN_DATE : String = "challanDate"
}

class InterDepotTransferVerification {
    static let STATUS : String = "status"
    static let PRODUCT_NAME : String = "productName"
    static let PRODUCT_CODE : String = "productCode"
    static let INDENT_NO : String = "indentNo"
    static let OFS_NUMBER : String = "ofsNumber"
    static let SOURCE_LOC_NAME : String = "sourceLocName"
    static let DEST_LOC_NAME : String = "destLocName"
    static let CONSIGNMENT_TYPE : String = "consignmentType"
    static let OFC_QTY : String = "ofsQty"
}

class TP_ID_CS_BTP_Verification {
    static let STATE : String = "state"
    static let STATUS : String = "status"
    static let SOURCE_NAME : String = "sourceName"
    static let PRODUCT_NAME : String = "productName"
    static let CREATED_BY : String = "createdBy"
    static let CREATED_ON : String = "createdOn"
    static let TP_NUMBER : String = "tpNumber"
    static let LR_NUMBER : String = "lrNumber"
    static let VEHICLE_NUMBER : String = "vehiclNumber"
    static let ISSUE_DATE : String = "issueDate"
    static let ISSUE_TIME : String = "issueTime"
    static let VALID_DATE : String = "validDate"
    static let VALID_TIME : String = "validTime"
    static let ROUTE : String = "route"
    static let SHIPMENT_ID : String = "shipmetId"
    static let LR_DATE : String = "lrDate"
    static let TRANSPORTER : String = "transporter"
    static let VALID_PERMIT_ID : String = "validPeriod"
    static let DEST_NAME : String = "destName"
    static let INTERIOR_ID : String = "intiatorId"
    static let APPROVAL_DATE : String = "approvalDate"
    static let QTY_IN_CASE : String = "departureDate"
    static let DEPARTURE_TIME : String = "departureTime"
    static let INVOICE_PROFORMA_ID : String = "invoiceProformaId"
    static let PRODUCT_CODE : String = "productCode"
    static let SIZE_CODE : String = "sizeCode"
    static let SHIPMENT_QTY : String = "shipmentQty"
    static let DC_NUMBER : String = "dcNumber"
    static let OUTLET_REG_CODE : String = "outletRegCode"
    static let DEPOT_NAME : String = "depotName"
    static let OUTLET_NAME : String = "outletName"
    static let AMT_TOTAL : String = "amtTotal"
}

class FetchCaseBarcodes {
    static let STATUS : String = "status"
    static let CASE_CODE : String = "caseCode"
}

class FetchHealBarcodes {
    static let STATUS : String = "status"
    static let HEAL_CODE : String = "healCode"
}

class EnforcementAPIKeys {
    class func RetailerInfoAPIKeys() -> [[String]] {
        return [["vendorName","Vendor Name"],
                ["vendorCode","Vendor Code"],
                ["region","Region"],
                ["exDistCode","ExDist Code"],
                ["revDistCode","RevDist Code"],
                ["apbclDepotCode","APBCL Depot Code"],
                ["retailerCode","Retailer Code"],
                ["gazetteCode","Gazette Code"],
                ["exCircleCode","ExCircle Code"],
                ["vendorTypeCode","Vendor Type Code"],
                ["vendorRegNo","Vendor Reg No"],
                ["licenseNo","License No"],
                ["licenseeName","License Name"],
                ["licenseIssueDate","License Issue Date"],
                ["licenseExpiryDate","License Expiry Date"],
                ["panNumber","PAN Number"],
                ["venderId","Vender ID"]]
    }
    
    class func BottleVerificationAPIKeys() -> [[String]] {
        return [["productName","Product Name"],
                ["series","Series"],
                ["updatedOn","Updated On"],
                ["productCode","Product Code"],
                ["sizeCode","Size Code"],
                ["caseCode","Case Code"],
                ["batchNumber","Batch Number"],
                ["manufacturingDate","Manufacturing Date"],
                ["manufacturinLocationName","Manufacturing Location Name"],
                ["depoLocationName","Depo Location Name"],
                ["vendorLocationName","Vendor Location Name"],
                ["currentLocationId","Current Location ID"],
                ["brandNumber","Brand Number"],
                ["purpose","Purpose"],
                ["mrp","MRP"],
                ["exDistCode","ExDist Code"],
                ["revDistCode","RevDist Code"],
                ["apbclDepotCode","APBCL Depot Code"],
                ["retailerCode","Retailer Code"],
                ["gazetteCode","Gazette Code"],
                ["exCircleCode","ExCircle Code"],
                ["shcode","SH Code"],
                ["packType","Pack Type"]]
    }
    
    class func CaseVerificationAPIKeys() -> [[String]] {
        return [["productName","Product Name"],
                ["productCode","Product Code"],
                ["sizeCode","Size Code"],
                ["caseCode","Case Code"],
                ["caseType","Case Type"],
                ["batchNumber","Batch Number"],
                ["lotNumber","Lot Number"],
                ["manufacturingDate","Manufacturing Date"],
                ["manufacturinLocationName","Manufacturin Location Name"],
                ["depoLocationName","Depo Location Name"],
                ["vendorLocationName","Vendor Location Name"],
                ["currentLocationId","Current Location ID"],
                ["unitsPerCase","Units Per Case"],
                ["brandNumber","Brand Number"],
                ["purpose","Purpose"],
                ["mrp","MRP"],
                ["distSyncDate","Dist Sync Date"],
                ["shipmentDate","Shipment Date"],
                ["exDistCode","ExDist Code"],
                ["revDistCode","RevDist Code"],
                ["apbclDepotCode","APBCL Depot Code"],
                ["retailerCode","Retailer Code"],
                ["gazetteCode","Gazette Code"],
                ["exCircleCode","ExCircle Code"]]
    }
    
    class func TransportThroughPermitAPIKeys() -> [[String]] {
        return [["createdBy","CreatedBy"],
                ["createdOn","CreatedOn"],
                ["route","Route"],
                ["importPermitNo","Import Permit No"],
                ["importPermitDate","Import Permit Date"],
                ["importPermitValidFrom","Import Permit Valid From"],
                ["importPermitValidTo","Import Permit Valid To"],
                ["importPermitIssuedBy","Import Permit Issued By"],
                ["consignmentOrigin","Consignment Origin"],
                ["exportPermitNo","Export Permit No"],
                ["exportPermitDate","Export Permit Date"],
                ["exportPermitIssuedBy","Export Permit Issued By"],
                ["consignmentDestination","Consignment Destination"],
                ["importerName","Importer Name"],
                ["exporterName","Exporter Name"],
                ["conveyanceMode","Conveyance Mode"],
                ["oldVehicleNo","Old Vehicle No"],
                ["thruPermitValidFrom","Thru Permit Valid From"],
                ["thruPermitValidTo","Thru Permit Valid To"],
                ["startCheckPoint","Start Check Point"],
                ["endCheckPoint","End Check Point"],
                ["throughPermitNo","Through Permit No"],
                ["transporterName","Transporter Name"],
                ["transporterAddress","Transporter Address"],
                ["newVehicleNo","New Vehicle No"],
                ["extenValTo","Exten Val To"],
                ["revalReqBy","Reval Req By"],
                ["intoxicantType","Intoxicant Type"],
                ["intoxicantBrand","Intoxicant Brand"],
                ["intoxicantMl","Intoxicant Ml"],
                ["intoxicantStrength","Intoxicant Strength"],
                ["intoxicantNoCases","Intoxicant No Cases"],
                ["intoxicantBl","Intoxicant Bl"],
                ["intoxicantPpl","Intoxicant Ppl"]]
    }
    
    class func ExportPermitVerificationAPIKeys() -> [[String]] {
        return [["state","State"],
                ["sourceName","Source Name"],
                ["productName","Product Name"],
                ["createdBy","Created By"],
                ["createdOn","Created On"],
                ["tpNumber","TP Number"],
                ["lrNumber","LR Number"],
                ["vehiclNumber","Vehicl Number"],
                ["issueDate","Issue Date"],
                ["issueTime","Issue Time"],
                ["validDate","Valid Date"],
                ["validTime","Valid Time"],
                ["route","Route"],
                ["shipmetId","Shipmet ID"],
                ["lrDate","LR Date"],
                ["transporter","Transporter"],
                ["validPeriod","Valid Period"],
                ["destName","DestName"],
                ["intiatorId","Intiator ID"],
                ["approvalDate","Approval Date"],
                ["departureDate","Departure Date"],
                ["departureTime","Departure Time"],
                ["invoiceProformaId","Invoice Proforma ID"],
                ["productCode","Product Code"],
                ["sizeCode","Size Code"],
                ["shipmentQty","Shipment Qty"],
                ["dcNumber","DC Number"],
                ["outletRegCode","Outlet Reg Code"],
                ["depotName","Depot Name"],
                ["outletName","Outlet Name"],
                ["amtTotal","Amount Total"]]
    }
    
    class func EventPermitVerificationAPIKeys() -> [[String]] {
        return [["issueDate","Issue Date"],
                ["permitNumber","Permit Number"],
                ["issuedBy","Issued By"],
                ["permitFee","Permit Fee"],
                ["paymentMode","Payment Mode"],
                ["instrumentNumber","Instrument Number"],
                ["bank","Bank"],
                ["instrumentDated","Instrument Dated"],
                ["eventDate","Event Date"],
                ["eventAddress","Event Address"],
                ["boundaryEast","Boundary East"],
                ["boundaryWest","Boundary West"],
                ["boundaryNorth","Boundary North"],
                ["boundarySouth", "Boundary South"]]
    }
    
    class func ImportPermitVerificationAPIKeys() -> [[String]] {
        return [["validityPeriod","Validity Period"],
                ["route","Route"],
                ["productCode","Product Code"],
                ["sizeCode","Size Code"],
                ["depotName","Depot Name"],
                ["bank","Bank"],
                ["permitNo","Permit No"],
                ["permitDate","Permit Date"],
                ["ponumber","PO Number"],
                ["podate","PO Date"],
                ["licenseeId","License ID"],
                ["intoxicantExporterLoc","Intoxicant Exporter Loc"],
                ["intoxicantExporterPlace","Intoxicant Exporter Place"],
                ["importPurpose","Import Purpose"],
                ["tpBarcode","TP Barcode"],
                ["importId","Import ID"],
                ["intoxicantExporterName","Intoxicant Exporter Name"],
                ["brandName","Brand Name"],
                ["sizeInMl","Size In Ml"],
                ["qtyInCases","Qty In Cases"],
                ["paymentHead","Payment Head"],
                ["challanNo","Challan No"],
                ["challanAmt","Challan Amount"],
                ["challanDate","Challan Date"]]
        
    }
    
    class func InterDepotTransferVerificationAPIKeys() -> [[String]] {
        return [["productName","Product Name"],
                ["productCode","Product Code"],
                ["indentNo","Indent No"],
                ["ofsNumber","OFS Number"],
                ["sourceLocName","Source Location Name"],
                ["destLocName","DestLocation Name"],
                ["consignmentType","Consignment Type"],
                ["ofsQty","OFS Qty"]]
    }
    
    class func TP_ID_CS_BTP_VerificationAPIKeys() -> [[String]] {
        return [["state","State"],
                ["sourceName","Source Name"],
//                ["productName","Product Name"],
                ["createdBy","Created By"],
                ["createdOn","Created On"],
                ["tpNumber","TP Number"],
                ["lrNumber","LR Number"],
                ["vehiclNumber","Vehicle Number"],
                ["issueDate","Issue Date"],
                ["issueTime","Issue Time"],
                ["validDate","Valid Date"],
                ["validTime","Valid Time"],
                ["route","Route"],
                ["shipmetId","Shipmet ID"],
                ["lrDate","LR Date"],
                ["transporter","Transporter"],
                ["validPeriod","ValidPeriod"],
                ["destName","Dest Name"],
                ["intiatorId","Intiator ID"],
                ["approvalDate","Approval Date"],
                ["departureDate","Departure Date"],
                ["departureTime","Departure Time"],
                ["invoiceProformaId","Invoice Proforma ID"],
//                ["productCode","Product Code"],
//                ["sizeCode","Size Code"],
//                ["shipmentQty","Shipment Qty"],
                ["dcNumber","DC Number"],
                ["outletRegCode","Outlet Reg Code"],
                ["depotName","Depot Name"],
                ["outletName","Outlet Name"],
                ["amtTotal","AmountTotal"]]
    }
    
    class func FetchCaseBarcodesAPIKeys() -> [String] {
        return ["status",
                "caseCode"]
    }
    
    class func FetchHealBarcodesAPIKeys() -> [String] {
        return ["status",
                "healCode"]
    }
}
