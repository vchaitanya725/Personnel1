//
//  EnforcementUtilClasses.swift
//  Enforcement
//
//  Created by Viswa Gopisetty on 06/05/17.
//  Copyright © 2017 Viswa Gopisetty. All rights reserved.
//

import UIKit
import MapKit

class LoadingIndicator {
    static let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    class func showActivityIndicatorOnWindow() {
        MBProgressHUD.hide(for: appDelegate.window!, animated: true)
        let HUD : MBProgressHUD = MBProgressHUD.showAdded(to: appDelegate.window!, animated: true)
        HUD.contentColor = EnforcementCommonMethods.defaultColor()
        HUD.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
    }
    
    class func removeActivityIndicatorOnWindow() {
        MBProgressHUD.hide(for: appDelegate.window!, animated: true)
    }

    class func showActivityIndicatorOnView(destination : UIView) {
        
    }
    
    class func removeActivityIndicatorOnView(destination : UIView) {
        
    }
}

class EnforcementUtils {
    static var defaultCordinate : CLLocation? {
        set {
            archieveUserDetailsAndSave(response: newValue!, key: EnforcementCommonConstants.DEFAULT_CORDINATE)
        }
        get {
            return unarchiveDetails(key: EnforcementCommonConstants.DEFAULT_CORDINATE) as? CLLocation
        }
    }
    
    static var defaultLocation : String? {
        set {
            UserDefaults.standard.setValue(newValue!, forKey: EnforcementCommonConstants.DEFAULT_LOCATION)
        }
        get {
            return UserDefaults.standard.value(forKey: EnforcementCommonConstants.DEFAULT_LOCATION) as? String
        }
    }
    
    static var defaultState : String? {
        set {
            UserDefaults.standard.setValue(newValue!, forKey: EnforcementCommonConstants.DEFAULT_STATE)
        }
        get {
            return UserDefaults.standard.value(forKey: EnforcementCommonConstants.DEFAULT_STATE) as? String
        }
    }
    
    static var userDefaultCredentials : NSDictionary? {
        set {
            archieveUserDetailsAndSave(response: newValue!, key: EnforcementCommonConstants.DEFAULT_CREDENTIALS)
        }
        get {
            return unarchiveDetails(key: EnforcementCommonConstants.DEFAULT_CREDENTIALS) as? NSDictionary
        }
    }
    
    static var isLoginSuccess : Bool? {
        set {
            UserDefaults.standard.setValue(newValue!, forKey: EnforcementCommonConstants.LOGIN_SUCCESS)
        }
        get {
            return UserDefaults.standard.value(forKey: EnforcementCommonConstants.LOGIN_SUCCESS) as? Bool
        }
    }
    
    class func archieveUserDetailsAndSave(response : AnyObject, key : String) {
        let keyArchiever : Data = NSKeyedArchiver.archivedData(withRootObject: response)
        UserDefaults.standard.set(keyArchiever, forKey: key)
    }
    
    class func unarchiveDetails(key : String) -> AnyObject? {
        let archievedData : Data? = UserDefaults.standard.value(forKey: key) as? Data
        if(archievedData != nil) {
            return NSKeyedUnarchiver.unarchiveObject(with: archievedData!) as AnyObject
        } else {
            return nil
        }
    }
}

class EnforcementCommonMethods {
    class func getState() -> String {
        let state : String? = EnforcementUtils.defaultState
        return "HPFS" + "AP"//(state ?? "TS")
    }
    
    class func scannedTypeType(with scannedCode: String) -> ScannedCodeType? {
        if(scannedCode != "") {
            if(scannedCode.hasPrefix("890")) {
                return .TransportPermitVerification
            } else if(scannedCode.hasPrefix("TTP")) {
                return .TransportPermitVerification
            } else if(scannedCode.hasPrefix("EX")) {
                return .ExportPermitVerification
            } else if(scannedCode.hasPrefix("EV")) {
                return .EventpermitVerification
            } else if(scannedCode.hasPrefix("IM")) {
                return .ImportPermitVerification
            } else if(scannedCode.hasPrefix("OFIDT")) {
                return .InterDepotTransferVerification
            } else if((scannedCode.hasPrefix("TP") || (scannedCode.hasPrefix("ID")) || (scannedCode.hasPrefix("CS")) || (scannedCode.hasPrefix("BTP")))) {
                return .TP_ID_CS_BTPVerification
            } else if(scannedCode.characters.count == 17) {
                return .BottleVerification
            } else if(scannedCode.characters.count <= 30) {
                return .RetailerInfo
            }
            
            return nil
        } else {
            return nil
        }
    }
    
    class func validateTypeOfCode(type : ScannedCodeType) -> [[String]] {
        switch type {
        case .RetailerInfo:
            return EnforcementAPIKeys.RetailerInfoAPIKeys()
        case .TP_ID_CS_BTPVerification:
            return EnforcementAPIKeys.TP_ID_CS_BTP_VerificationAPIKeys()
        case .InterDepotTransferVerification:
            return EnforcementAPIKeys.InterDepotTransferVerificationAPIKeys()
        case .ImportPermitVerification:
            return EnforcementAPIKeys.ImportPermitVerificationAPIKeys()
        case .EventpermitVerification:
            return EnforcementAPIKeys.EventPermitVerificationAPIKeys()
        case .ExportPermitVerification:
            return EnforcementAPIKeys.ExportPermitVerificationAPIKeys()
        case .TransportPermitVerification:
            return EnforcementAPIKeys.TransportThroughPermitAPIKeys()
        case .CaseVerification:
            return EnforcementAPIKeys.CaseVerificationAPIKeys()
        case .BottleVerification:
            return EnforcementAPIKeys.BottleVerificationAPIKeys()
        }
    }
    
    class func defaultColor() -> UIColor {
        return UIColor(red: 0/255, green: 178/255, blue: 203/255, alpha: 1.0)
    }
    
    class func getViewControllerWithId(storyBoardId : String) -> AnyObject {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: storyBoardId)
    }
    
    class func saveAddressForLocation(location : CLLocation, completionHandler : ((Void)->(Void))?) {
        EnforcementUtils.defaultCordinate = location

        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            if(placemarks != nil) {
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let placemark = placemarks! as [CLPlacemark]
                
                if placemark.count > 0 {
                    let placemark = placemarks![0]
                    
                    var addressString : String = ""
                    if placemark.subLocality != nil {
                        addressString = addressString + placemark.subLocality! + ", "
                    }
                    if placemark.thoroughfare != nil {
                        addressString = addressString + placemark.thoroughfare! + ", "
                    }
                    if placemark.locality != nil {
                        addressString = addressString + placemark.locality! + ", "
                    }
                    if placemark.country != nil {
                        addressString = addressString + placemark.country! + ", "
                    }
                    if placemark.postalCode != nil {
                        addressString = addressString + placemark.postalCode! + " "
                    }
                    
                    EnforcementUtils.defaultLocation = addressString
                    
                    if(placemarks != nil) {
                        var state : String = placemarks![0].administrativeArea!
                        if(state == "TG") {
                            state = "TS"
                        }
                        if(state != "TS" && state != "AP") {
                            state = "TS"
                        }
                        EnforcementUtils.defaultState = state
                    } else {
                        EnforcementUtils.defaultState = "AP"
                    }
                }
                
                if(completionHandler != nil) {
                    completionHandler!()
                }
            }
        })
    }
}

class IMEIClass {
    static let DEVICE_IMEI : String = "dev_imei"

    class func getUUID() -> String {
        let theUUID: CFUUID = CFUUIDCreate(nil)
        let string: CFString = CFUUIDCreateString(nil, theUUID)
        return string as String
    }
    
    class func verifyOrGenerateUUID() {
        var stringApplicationUUID:String? = SSKeychain.password(forService: "Pay2U", account: getAPPBundleName())
        if stringApplicationUUID == nil {
            stringApplicationUUID = self.getUUID()
            SSKeychain.setPassword(stringApplicationUUID, forService: "Pay2U", account: getAPPBundleName())
        }
        UserDefaults.standard.setValue(stringApplicationUUID, forKey: self.DEVICE_IMEI)
    }
    
    class func getAPPBundleName()->String {
        let bundleIdentifier:String? = Bundle.main.bundleIdentifier
        return bundleIdentifier!
    }
}

extension UITextField {
    func setUnderlined(with color: UIColor) {
        let layer : CALayer? = self.layer.sublayers?.filter { $0.name == "underline"}.first
        layer?.removeFromSuperlayer()
        
        let border = CALayer()
        border.name = "underline"
        
        let width = CGFloat(1.0)
        border.borderColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
    
    func md5Encryption() -> String! {
        let str = self.cString(using: .utf8)
        let strLen = CUnsignedInt(self.lengthOfBytes(using: .utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        
        CC_MD5(str!, strLen, result)
        
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        result.deinitialize()
        
        return String(format: hash as String)
    }
}

extension NSDictionary {
    func checkForValidStatus() -> Bool {
        if(self.value(forKey: EnforcementCommonConstants.STATUS) != nil && self.value(forKey: EnforcementCommonConstants.STATUS) as! String == "true") {
            return true
        } else {
            return false
        }
    }
}

extension UIImage {
    func imageWithColor(color: UIColor) -> UIImage? {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
