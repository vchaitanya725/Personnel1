//
//  VeritAPI.swift
//  Verit
//
//  Created by Viswa Gopisetty on 06/05/17.
//  Copyright © 2017 Viswa Gopisetty. All rights reserved.
//

import Foundation

struct EnforcementAPI {
    /*
     username : ES_PALASA
     pwd: 21232f297a57a5a743894a0e4a801fc3
     
     vendor: 17018035061242M012B000083
     
     sh: 16711412013321424
     
     case: 8901611W0110GNN26829011509F051
     
     tp: TTP2016103168
     
     etp: EX1671300815AP100121
     
     shipment: EVP20163
     
     ofitd : OFIDTAIC47AC05757
     
     btp: TP168520031500016
     
     f barcodes: TP164121121501328, pc : 1641R0489GNN
     c barcode : 8901611W0110GNN268290115007CF1
     
 */
    //STATE is getting appended for every API in APIManager
    
    static let BASE_URL = "http://183.82.106.234:8001/"
    
    static let APPEND_TP_URL = BASE_URL + "EnforcementApp/REST/"
    
    static let SOA_URL = BASE_URL + "soa-infra/services/default/UpdateService/process?operationName=%@&LocationId=%@&LocationType=%@&SourceLocation=%@&ObjectType=%@&Payload=%@"
    
    static let USER_AUTHENTICATION = APPEND_TP_URL + "EnforcementLoginService/login?USER=%@&PWD=%@&"
    
    static let GET_RETAILER_INFO = APPEND_TP_URL + "EnforcementVenderService/vendor?VENDOR=%@&"
    
    static let BOTTLE_VERIFICATION = APPEND_TP_URL + "ShVerificationService/sh?SH=%@&"
    
    static let CASE_VERIFICATION = APPEND_TP_URL + "CaseVerificationService/case?CASE=%@&"
    
    static let TRANSPORT_PERMIT_VERIFICATION = APPEND_TP_URL + "TTPService/tp?TP=%@&"
    
    static let EXPORT_PERMIT_VERIFICATION = APPEND_TP_URL + "EnforcementTPService/tp?TP=%@&"
    
    static let EVENT_PERMIT_VERIFICATION = APPEND_TP_URL + "EnforcementEVPservice/shipment?SHIPMENT=&"
    
    static let IMPORT_PERMIT_VERIFICATION = APPEND_TP_URL + "EnforcementImportPermitService/tp?TP=IM168315361&"
    
    static let INTER_DEPOT_TRANSFER_VERIFICATION = APPEND_TP_URL + "EnforcementOFIDTPermit/ofidt?OFIDT=%@&"
    
    static let TP_ID_CS_BTP_VERIFICATION = APPEND_TP_URL + "EnforcementTPService/tp?TP=%@&"
    
    static let FETCH_CASE_BARCODES = APPEND_TP_URL + "EnforcementFetchCasebarcode/fetchcase?TP=%@&PRODUCTCODE=%@&"
    
    static let FETCH_HEAL_BARCODES = APPEND_TP_URL + "EnforcementFetchHealcode/fetchheal?CASEBARCODE=%@&"
}







