//
//  HealAndCaseCodeViewController.swift
//  Enforcement
//
//  Created by Viswa Gopisetty on 27/07/17.
//  Copyright © 2017 Viswa Gopisetty. All rights reserved.
//

import UIKit

class HealAndCaseCodeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var TP_Code : String! = nil
    
    let PRODUCT_LIST : Int = 0
    let CASE_CODE : Int = 1
    let HEAL_CODE : Int = 2
    let SPECIFIC_FIELD_LIST : Int = 3
    
    var codes: [String]! = nil
    var codeType : Int! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return codes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var tableViewCell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "detailCell")
        
        if(tableViewCell == nil) {
            tableViewCell = UITableViewCell(style: .subtitle, reuseIdentifier: "detailCell")
        }
        
        let titleString : String = "\(indexPath.row + 1)   " + codes[indexPath.row]
        let heightForString : CGFloat = titleString.heightWithConstrainedWidth(width: tableView.frame.width, font: UIFont.systemFont(ofSize: 12))
        
        tableViewCell?.textLabel?.numberOfLines = 0
        tableViewCell?.detailTextLabel?.textColor = UIColor.lightGray
        tableViewCell?.accessoryType = .detailButton
        
        tableViewCell?.textLabel?.frame = CGRect(x: 10, y: 20, width: tableView.frame.width - 20, height: heightForString)
        
        tableViewCell?.textLabel?.text = titleString
        
        tableViewCell?.selectionStyle = .none
        return tableViewCell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(codeType == PRODUCT_LIST) {
            LoadingIndicator.showActivityIndicatorOnWindow()
            APIManager.sharedInstance.fetchCaseBarcodes(tpCode: TP_Code , productCode: codes[indexPath.row], success: { (response : Any?) -> (Void) in
                let responseDic : NSDictionary = (response as? NSDictionary)!
                let isValid : String? = responseDic.value(forKey: "status") as? String
                
                if(isValid != nil && isValid == "false") {
                    _ = SweetAlert().showAlert("Invalid Code", subTitle: "Looks like code doesn't contain any data...", style: .warning)
                } else {
                    let caseCodeViewController : HealAndCaseCodeViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HealAndCaseCodeViewController") as! HealAndCaseCodeViewController
                    caseCodeViewController.codeType = self.CASE_CODE
                    caseCodeViewController.codes = responseDic.value(forKey: FetchCaseBarcodes.CASE_CODE) as! [String]
                    self.navigationController?.pushViewController(caseCodeViewController, animated: true)
                }
                
                LoadingIndicator.removeActivityIndicatorOnWindow()
            }) { (error : Error?) -> (Void) in
                _ = SweetAlert().showAlert("Error", subTitle: "Could not find product details", style: .warning)
                LoadingIndicator.removeActivityIndicatorOnWindow()
            }
        } else if(codeType == CASE_CODE) {
            LoadingIndicator.showActivityIndicatorOnWindow()
            APIManager.sharedInstance.fetchHealBarcodes(productCode: codes[indexPath.row], success: { (response : Any?) -> (Void) in
                let responseDic : NSDictionary = (response as? NSDictionary)!
                let isValid : String? = responseDic.value(forKey: EnforcementCommonConstants.STATUS) as? String
                
                if(isValid != nil && isValid == "false") {
                    _ = SweetAlert().showAlert("Invalid Barcode", subTitle: "Looks like code doesn't contain any data...", style: .warning)
                } else {
                    let caseCodeViewController : HealAndCaseCodeViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HealAndCaseCodeViewController") as! HealAndCaseCodeViewController
                    caseCodeViewController.codeType = self.HEAL_CODE
                    caseCodeViewController.codes = responseDic.value(forKey: FetchHealBarcodes.HEAL_CODE) as! [String]
                    self.navigationController?.pushViewController(caseCodeViewController, animated: true)
                }
                
                LoadingIndicator.removeActivityIndicatorOnWindow()
            }) { (error : Error?) -> (Void) in
                _ = SweetAlert().showAlert("Error", subTitle: "Could not find product details", style: .warning)
                LoadingIndicator.removeActivityIndicatorOnWindow()
            }
        } else if(codeType == HEAL_CODE) {
            LoadingIndicator.showActivityIndicatorOnWindow()
            APIManager.sharedInstance.fetchDetailsForCode(scannedCode: codes[indexPath.row], success: { (response : Any?) -> (Void) in
                let responseDic : NSDictionary = (response as? NSDictionary)!
                let isValid : String? = responseDic.value(forKey: EnforcementCommonConstants.STATUS) as? String
                
                if(isValid != nil && isValid == "false") {
                    _ = SweetAlert().showAlert("Invalid Barcode", subTitle: "This barcode doesn't contain any data. Try another..", style: .warning)
                } else {
                    let productDetails : ProductDetailsViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productDetailVC") as! ProductDetailsViewController
                    productDetails.APIKeysToDisplayForCode = EnforcementCommonMethods.validateTypeOfCode(type: EnforcementCommonMethods.scannedTypeType(with: self.codes[indexPath.row])!)
                    productDetails.scannedCode = self.codes[indexPath.row]
                    productDetails.APIResponse = responseDic
                    self.navigationController?.pushViewController(productDetails, animated: true)
                }
                
                LoadingIndicator.removeActivityIndicatorOnWindow()
            }) { (error : Error?) -> (Void) in
                _ = SweetAlert().showAlert("Error", subTitle: "Could not find product details", style: .warning)
                LoadingIndicator.removeActivityIndicatorOnWindow()
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
