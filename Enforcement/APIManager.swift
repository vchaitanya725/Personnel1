//
//  APIManager.swift
//  pay2u
//
//  Created by Phani on 10/04/17.
//  Copyright © 2017 Viswa. All rights reserved.
//

import UIKit
import ReachabilitySwift
import Alamofire

class APIManager: AlamofireLayer{
    private let reachability : Reachability = Reachability()!
    
    class var sharedInstance: APIManager {
        struct Static {
            static let instance = APIManager(configuration: URLSessionConfiguration.default)
        }
        
        return Static.instance
    }
    
    // USER AUTHENTICATION
    
    func userAuthentication(username: String,password : String, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))){
        self.getRequestWithStateAppended(partUrl: String(format: EnforcementAPI.USER_AUTHENTICATION, username, password), flagStatusCode: false, flagNetworkFailureAlert: true, success: success, failure: failure)
    }
    
    func fetchDetailsForCode(scannedCode : String, success:@escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))) {
        let barCodeType : ScannedCodeType? = EnforcementCommonMethods.scannedTypeType(with: scannedCode)
        if(barCodeType != nil) {
            switch barCodeType! {
            case .RetailerInfo:
                self.retailerInfo(vendor: scannedCode, success: success, failure: failure)
            case .BottleVerification:
                self.bottleVerification(sh: scannedCode, success: success, failure: failure)
            case .CaseVerification:
                self.caseVerification(caseId: scannedCode, success: success, failure: failure)
            case .TransportPermitVerification:
                self.transportPermitVerification(permit: scannedCode, success: success, failure: failure)
            case .ExportPermitVerification:
                self.exportPermitVerification(permit: scannedCode, success: success, failure: failure)
            case .ImportPermitVerification:
                self.importPermitVerification(permit: scannedCode, success: success, failure: failure)
            case .EventpermitVerification:
                self.eventPermitVerification(permit: scannedCode, success: success, failure: failure)
            case .InterDepotTransferVerification:
                self.interDepotTransferVerification(permit: scannedCode, success: success, failure: failure)
            case .TP_ID_CS_BTPVerification:
                self.TP_ID_CS_BTPVerification(permit: scannedCode, success: success, failure: failure)
            }
        } else {
            failure(nil)
        }
        
    }
    
    func inspectionOrBookCrime(inspectionOrCrime: Bool, payload: String, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))) {
        var preparedAPI : String
        if(inspectionOrCrime) {
            preparedAPI = String(format: EnforcementAPI.SOA_URL, "Request-Response", "DEVICE ID","HANDHELD", EnforcementCommonMethods.getState(), "VERIFICATION", payload)
        } else {
            preparedAPI = String(format: EnforcementAPI.SOA_URL, "Request-Response", "DEVICE ID","HANDHELD", EnforcementCommonMethods.getState(), "CRIME", payload)
        }
        
        let user = "qauser" //"ProdTestUser"
        let password = "password1" //"password1"
        let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        let headers = ["Authorization": "Basic \(base64Credentials)"]
        
        self.getRequest(preparedAPI, urlHeaders: headers, checkStatusCode: false, canShowNetworkFailureAlert: true, success: success, failure: failure)
    }
    
    // GET RETAILER INFORMATION
    
    func retailerInfo(vendor: String, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))){
        self.getRequestWithStateAppended(partUrl: String(format: EnforcementAPI.GET_RETAILER_INFO, vendor), flagStatusCode: false, flagNetworkFailureAlert: true, success: success, failure: failure)
    }
    
    // BOTTLE VERIFICATION
    
    func bottleVerification(sh: String, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))){
        self.getRequestWithStateAppended(partUrl: String(format: EnforcementAPI.BOTTLE_VERIFICATION, sh), flagStatusCode: false, flagNetworkFailureAlert: true, success: success, failure: failure)
    }
    
    // CASE VERIFICATION
    
    func caseVerification(caseId: String, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))){
        self.getRequestWithStateAppended(partUrl: String(format: EnforcementAPI.CASE_VERIFICATION, caseId), flagStatusCode: false, flagNetworkFailureAlert: true, success: success, failure: failure)
    }
    
    // TRANSPORT PERMIT VERIFICATION
    
    func transportPermitVerification(permit: String, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))){
        self.getRequestWithStateAppended(partUrl: String(format: EnforcementAPI.TRANSPORT_PERMIT_VERIFICATION, permit), flagStatusCode: false, flagNetworkFailureAlert: true, success: success, failure: failure)
    }
    
    // EXPORT VERIFICATION
    
    func exportPermitVerification(permit: String, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))){
        self.getRequestWithStateAppended(partUrl: String(format: EnforcementAPI.EXPORT_PERMIT_VERIFICATION, permit), flagStatusCode: false, flagNetworkFailureAlert: true, success: success, failure: failure)
    }
    
    // EVENT PERMIT VERIFICATION
    
    func eventPermitVerification(permit: String, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))){
        self.getRequestWithStateAppended(partUrl: String(format: EnforcementAPI.EVENT_PERMIT_VERIFICATION, permit), flagStatusCode: false, flagNetworkFailureAlert: true, success: success, failure: failure)
    }
    
    // IMPORT PERMIT VERIFICATION
    
    func importPermitVerification(permit: String, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))){
        self.getRequestWithStateAppended(partUrl: String(format: EnforcementAPI.IMPORT_PERMIT_VERIFICATION, permit), flagStatusCode: false, flagNetworkFailureAlert: true, success: success, failure: failure)
    }
    
    // INTER DEPOT TRANSFER VERIFICATION
    
    func interDepotTransferVerification(permit: String, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))){
        self.getRequestWithStateAppended(partUrl: String(format: EnforcementAPI.INTER_DEPOT_TRANSFER_VERIFICATION, permit), flagStatusCode: false, flagNetworkFailureAlert: true, success: success, failure: failure)
    }
    
    // BTP VERIFICATION
    
    func TP_ID_CS_BTPVerification(permit: String, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))){
        self.getRequestWithStateAppended(partUrl: String(format: EnforcementAPI.TP_ID_CS_BTP_VERIFICATION, permit), flagStatusCode: false, flagNetworkFailureAlert: true, success: success, failure: failure)
    }
    
    // FETCH CASE BARCODES
    
    func fetchCaseBarcodes(tpCode:String, productCode: String, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))){
        self.getRequestWithStateAppended(partUrl: String(format: EnforcementAPI.FETCH_CASE_BARCODES, tpCode, productCode), flagStatusCode: false, flagNetworkFailureAlert: true, success: success, failure: failure)
    }
    
    // FETCH HEAL BARCODES
    
    func fetchHealBarcodes(productCode: String, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))){
        self.getRequestWithStateAppended(partUrl: String(format: EnforcementAPI.FETCH_HEAL_BARCODES, productCode), flagStatusCode: false, flagNetworkFailureAlert: true, success: success, failure: failure)
    }
    
    // APPEND STATE TO EVERY API
    
    func getRequestWithStateAppended(partUrl: String, flagStatusCode : Bool, flagNetworkFailureAlert: Bool, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))) {
        self.getRequest(self.appendState(url: partUrl), urlHeaders: nil, checkStatusCode: flagStatusCode, canShowNetworkFailureAlert: flagNetworkFailureAlert, success: success, failure: failure)
    }
    
    // APPEND STATE TO STRING
    
    func appendState(url : String) -> String {
        return url + "STATE=" + EnforcementCommonMethods.getState()
    }
    
}
