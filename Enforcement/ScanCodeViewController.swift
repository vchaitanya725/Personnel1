//
//  ScanCodeViewController.swift
//  Enforcement
//
//  Created by PAVANI VEMPATI on 16/07/17.
//  Copyright © 2017 Viswa Gopisetty. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import ReachabilitySwift

class ScanCodeViewController: UIViewController, DidFinishBarCodeScan {

    @IBOutlet var getDataButton: CustomButton!
    @IBOutlet var scanCodeTextField: JVFloatLabeledTextField!
    var scannedBarCode : String? = nil
    let reachability : Reachability = Reachability()!
    var isShowingKeyboard : Bool! = false

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        self.title = "Scan Barcode"
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
        
        scanCodeTextField.setUnderlined(with: EnforcementCommonMethods.defaultColor())

        getDataButton.onClickOfButton = {
            if(self.scannedBarCode != nil && EnforcementCommonMethods.scannedTypeType(with: self.scannedBarCode!) == nil) {
                _ = SweetAlert().showAlert("Error", subTitle: "Please enter valid barcode..", style: .warning)
            } else {
                self.scannedBarCode = self.scanCodeTextField.text!
                if (self.reachability.isReachable) {
                    self.fetchProductDetails()
                } else {
                    _ = SweetAlert().showAlert("Network Error..", subTitle: "Would you like to send offline message" , style: AlertStyle.warning, buttonTitle:"No", buttonColor:UIColor.red , otherButtonTitle:  "Yes!", otherButtonColor: EnforcementCommonMethods.defaultColor()) { (isOtherButton) -> Void in
                        if isOtherButton == false {
                            self.dismssKeyboard()
                        }
                    }
                }
            }
        }
        
        // Do any additional setup after loading the view.
    }

    override func viewWillLayoutSubviews() {
        if(self.navigationController?.isNavigationBarHidden)! {
            self.navigationController?.isNavigationBarHidden = false;
        }
        scanCodeTextField.setUnderlined(with: EnforcementCommonMethods.defaultColor())
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickOfCamera(_ sender: Any) {
        self.dismssKeyboard()
        let barcodeView :ScanBarCodeViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BarCodeView") as! ScanBarCodeViewController
        barcodeView.delegate = self
        self.present(barcodeView, animated: true, completion: nil)
    }
    
    func fetchProductDetails() {
        if(self.scannedBarCode != nil) {
            LoadingIndicator.showActivityIndicatorOnWindow()
            APIManager.sharedInstance.fetchDetailsForCode(scannedCode: self.scannedBarCode!, success: { (response : Any?) -> (Void) in
                let responseDic : NSDictionary = (response as? NSDictionary)!
                let isValid : String? = responseDic.value(forKey: EnforcementCommonConstants.STATUS) as? String
                
                if(isValid != nil && isValid == "false") {
                    _ = SweetAlert().showAlert("Invalid Barcode", subTitle: "This barcode doesn't contain any data. Try another..", style: .warning)
                } else {
                    let productDetails : ProductDetailsViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productDetailVC") as! ProductDetailsViewController
                    productDetails.APIKeysToDisplayForCode = EnforcementCommonMethods.validateTypeOfCode(type: EnforcementCommonMethods.scannedTypeType(with: self.scannedBarCode!)!)
                    productDetails.scannedCode = self.scannedBarCode!
                    productDetails.APIResponse = responseDic
                    self.navigationController?.pushViewController(productDetails, animated: true)
                }
                
                self.scanCodeTextField.text = ""
                self.scannedBarCode = nil
                LoadingIndicator.removeActivityIndicatorOnWindow()
            }) { (error : Error?) -> (Void) in
                _ = SweetAlert().showAlert("Error", subTitle: "Could not find product details", style: .warning)
                LoadingIndicator.removeActivityIndicatorOnWindow()
            }
        }
    }
    
    //MARK:- tap gesture
    
    func handleTap(_ sender :UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    
    func dismssKeyboard() {
        self.scanCodeTextField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        
        if(newString.length > 17) {
            return false
        }
        
        return true
    }
    
    func didFinishScanningBarCode(_ code : String) {
        self.scannedBarCode = code
        self.scanCodeTextField.text = code
        if(self.scannedBarCode != nil && EnforcementCommonMethods.scannedTypeType(with: self.scannedBarCode!) == nil) {
            _ = SweetAlert().showAlert("Error", subTitle: "Not valid barcode..", style: .warning)
            self.scanCodeTextField.text = ""
            self.scannedBarCode = nil
        } else {
            if (reachability.isReachable) {
                self.fetchProductDetails()
            } else {
                _ = SweetAlert().showAlert("Network Error..", subTitle: "Would you like to send offline message" , style: AlertStyle.warning, buttonTitle:"No", buttonColor:UIColor.red , otherButtonTitle:  "Yes!", otherButtonColor: EnforcementCommonMethods.defaultColor()) { (isOtherButton) -> Void in
                    if isOtherButton == false {
                        self.dismssKeyboard()
                    }
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
