//
//  CustomButton.swift
//  sample
//
//  Created by Viswa on 6/13/17.
//  Copyright © 2017 Viswa. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    @IBInspectable var borderColor: UIColor? {
        set {
            self.layer.borderColor = borderColor?.cgColor
        }
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
    }
    
    @IBInspectable var touchColor : UIColor!
    var touchBackUpColor : UIColor!
    
    var onClickOfButton : ((Void)->(Void))!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.touchBackUpColor = self.backgroundColor
        self.backgroundColor = self.touchColor
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.resetHighlight()
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        self.resetHighlight()
    }
    
    func resetHighlight() {
        UIView.animate(withDuration: 0.5, delay: 0, options: [.beginFromCurrentState,.allowUserInteraction], animations: {
            self.backgroundColor = self.touchBackUpColor
        }) { (value : Bool) in
            self.onClickOfButton()
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
