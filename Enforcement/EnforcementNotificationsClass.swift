//
//  EnforcementNotificationsClass.swift
//  Enforcement
//
//  Created by Viswa on 2/16/17.
//  Copyright © 2017 Viswa. All rights reserved.
//

import UIKit

struct EnforcementNotificationsClass {
    static let LOGGED_IN_SUCCESS : String = "LOGGED_IN_SUCCESS"
    static let LOGGED_OUT_SUCCESS = "LOGGED_OUT_SUCCESS"
    static let REFRESH_PROFILE_INFO = "REFRESH_PROFILE_INFO"
    
    static let KEYBOARD_DISMISS_NOTIFICATIONS = "dismissed_keyboard"

}
