//
//  ProductDetailsViewController.swift
//  Enforcement
//
//  Created by Viswa on 7/18/17.
//  Copyright © 2017 Viswa Gopisetty. All rights reserved.
//

import UIKit

class ProductDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tpCode: UIButton!
    
    var scannedCode : String! = nil
    var APIKeysToDisplayForCode : [[String]]! = nil
    var APIResponse : NSDictionary! = nil
    
    @IBOutlet var detailsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.checkAndDisableTPCode()
        
        self.filterKeysAvailable()
        
        self.detailsTableView.reloadData()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkAndDisableTPCode() {
        let scannedCodeType : ScannedCodeType? = EnforcementCommonMethods.scannedTypeType(with: self.scannedCode!)
        if(scannedCodeType != nil && scannedCodeType == .TP_ID_CS_BTPVerification) {
            tpCode.isHidden = false
        }
    }
    
    func filterKeysAvailable() {
        for keys in APIKeysToDisplayForCode {
            if(APIResponse.value(forKey: keys.first!) as? NSNull != nil) {
                APIKeysToDisplayForCode.remove(at: APIKeysToDisplayForCode.index(where: {$0 == keys})!)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return APIKeysToDisplayForCode.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var tableViewCell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "detailCell")
        
        if(tableViewCell == nil) {
            tableViewCell = UITableViewCell(style: .subtitle, reuseIdentifier: "detailCell")
        }
        
        var titleString : String
        var heightForString : CGFloat
        var subtitleString : String
        
        if(APIResponse.value(forKey: APIKeysToDisplayForCode[indexPath.row][0]) as? String == nil) {
            titleString = APIKeysToDisplayForCode[indexPath.row][1]
            heightForString = titleString.heightWithConstrainedWidth(width: tableView.frame.width, font: UIFont.systemFont(ofSize: 12))
            subtitleString = ""
        } else {
            titleString = APIResponse.value(forKey: APIKeysToDisplayForCode[indexPath.row][0]) as! String
            heightForString = titleString.heightWithConstrainedWidth(width: tableView.frame.width, font: UIFont.systemFont(ofSize: 12))
            subtitleString = APIKeysToDisplayForCode[indexPath.row][1]
        }
        
        tableViewCell?.textLabel?.numberOfLines = 0
        tableViewCell?.detailTextLabel?.textColor = UIColor.lightGray
        
        tableViewCell?.textLabel?.frame = CGRect(x: 10, y: 20, width: tableView.frame.width - 20, height: heightForString)
        tableViewCell?.detailTextLabel?.frame = CGRect(x: 10, y: heightForString + 20, width: tableView.frame.width - 20, height: subtitleString.heightWithConstrainedWidth(width: tableView.frame.width, font: UIFont.systemFont(ofSize: 10)))
        
        tableViewCell?.textLabel?.text = titleString
        tableViewCell?.detailTextLabel?.text = subtitleString.capitalized
        
        tableViewCell?.selectionStyle = .none
        return tableViewCell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let titleString : String = APIResponse.value(forKey: APIKeysToDisplayForCode[indexPath.row][0]) as! String
        let heightForString : CGFloat = titleString.heightWithConstrainedWidth(width: tableView.frame.width, font: UIFont.systemFont(ofSize: 25))
        return heightForString + 20
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(APIResponse.value(forKey: APIKeysToDisplayForCode[indexPath.row][0]) as? String == nil) {
            let caseCodeViewController : HealAndCaseCodeViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HealAndCaseCodeViewController") as! HealAndCaseCodeViewController
            caseCodeViewController.codeType = 3
            caseCodeViewController.codes = APIResponse.value(forKey: APIKeysToDisplayForCode[indexPath.row][0]) as! [String]
            self.navigationController?.pushViewController(caseCodeViewController, animated: true)
        }
    }
    
    @IBAction func onClickOfCaseCodes(_ sender: Any) {
        let caseCodeViewController : HealAndCaseCodeViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HealAndCaseCodeViewController") as! HealAndCaseCodeViewController
        caseCodeViewController.codeType = 0
        caseCodeViewController.TP_Code = self.scannedCode!
        caseCodeViewController.codes = APIResponse.value(forKey: TP_ID_CS_BTP_Verification.PRODUCT_CODE) as! [String]
        self.navigationController?.pushViewController(caseCodeViewController, animated: true)
    }
    
    @IBAction func onClickOfBookCrime(_ sender: Any) {
//        {"CrimeRegisters":{"CrimeRegister":{"Id":"TP168527021500005","SerialNo":"","CORNo":"","CORDate":"","Section":"","DetectingAgency":"","DetectingOfficerUserId":"AP_COMM","DetectingOfficerName":"Mr. Mukesh","AccusedName":"","AccusedAddress":"","ContrabandSeized":"","PropertyDepositedTo":"","DepositDate":"","CESamples":"","CENo":"","ChargeSheetDate":"2017-08-10T14:57:04","SRNo":"","CalenderCaseNo":"","CreatedBy":"Mr. Mukesh","CreatedOn":"2017-08-10T14:57:04","UpdatedBy":"","UpdatedOn":"","Remarks":"crime","CrimeType":"Route Deviation","Latitude":"17.4411998","Longitude":"78.398098"}}}
    }
    
    @IBAction func onClickOfInspection(_ sender: Any) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
