//
//  AlamofireLayer.swift
//  pay2u
//
//  Created by Phani on 10/04/17.
//  Copyright © 2017 Viswa. All rights reserved.
//

import Foundation
import UIKit
import ReachabilitySwift
import Alamofire

class AlamofireLayer: SessionManager
{
    private let reachability : Reachability = Reachability()!
    
    // GET REQUEST HANDLER
    
    func getRequest(_ url : String, urlHeaders: [String: String]?,  checkStatusCode : Bool, canShowNetworkFailureAlert : Bool, success : @escaping ((Any?)->(Void)), failure : @escaping ((Error?)->(Void))) {
        if (reachability.isReachable) {
            self.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: urlHeaders).responseJSON { (response:DataResponse<Any>) in
                if(response.result.error == nil) {
                    success(response.result.value as AnyObject)
                } else {
                    failure(response.result.error)
                }
            }
        } else {
            failure(nil)
            if (canShowNetworkFailureAlert) {
                self.showNetworkAlert()
            }
        }
    }
    
    // POST REQUEST HANDLER
    
    func postRequest(_ url : String, checkStatusCode : Bool, canShowNetworkFailureAlert : Bool, parametersDic : NSDictionary, success : @escaping ((AnyObject!)->(Void)), failure : @escaping ((Error?)->(Void))) {
        if (reachability.isReachable) {
            self.request(url, method: .post, parameters: parametersDic as? Parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in

                if(response.result.error == nil) {
                    success(response.result.value as AnyObject)
                } else {
                    failure(response.result.error)
                }
            }
        } else {
            failure(nil)
            if (canShowNetworkFailureAlert) {
                self.showNetworkAlert()
            }
        }
    }
    
    // PUT REQUEST HANDLER
    
    func putRequest(_ url : String, checkStatusCode : Bool, canShowNetworkFailureAlert : Bool, success : @escaping ((AnyObject!)->(Void)), failure : @escaping ((Error?)->(Void))) {
        if (reachability.isReachable) {
            self.request(url, method: .put, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                if(response.result.error == nil) {
                    success(response.result.value as AnyObject)
                } else {
                    failure(response.result.error)
                }
            }
        } else {
            failure(nil)
            if (canShowNetworkFailureAlert) {
                self.showNetworkAlert()
            }
        }
    }
    
    // DELETE REQUEST HANDLER
    
    func deleteRequest(_ url : String, checkStatusCode : Bool, canShowNetworkFailureAlert : Bool, success : @escaping ((AnyObject!)->(Void)), failure : @escaping ((Error?)->(Void)))
    {
        if (reachability.isReachable) {
            self.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                if(response.result.error == nil) {
                    success(response.result.value as AnyObject)
                } else {
                    failure(response.result.error)
                }
            }
        } else {
            failure(nil)
            if (canShowNetworkFailureAlert) {
                self.showNetworkAlert()
            }
        }
    }
    
    // NETWORK FAILURE HANDLER
    
    func showNetworkAlert() {
        _ = SweetAlert().showAlert("Network Error", subTitle: "Network Error. Please check your internet connection.", style: .warning)
    }
    
    // REQUEST FAILURE HANDLER
    
    func requestFailure() {
        _ = SweetAlert().showAlert("Alert", subTitle: "Something went wrong..!", style: .warning)
    }
}
