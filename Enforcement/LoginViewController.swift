//
//  LoginViewController.swift
//  Enforcement
//
//  Created by Viswa Gopisetty on 14/06/17.
//  Copyright © 2017 Viswa Gopisetty. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet var userName: UITextField!
    @IBOutlet var password: UITextField!
    
    @IBOutlet var loginButton: CustomButton!
    @IBOutlet var checkBox: VKCheckbox!
    
    var isShowingKeyboard : Bool! = false

    override func viewDidLoad() {
        super.viewDidLoad()

        checkBox.line             = .thin
        checkBox.bgColorSelected  = UIColor.clear
        checkBox.bgColor          = UIColor.clear
        checkBox.color            = UIColor.white
        checkBox.borderColor      = UIColor.white
        checkBox.borderWidth      = 1
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissKeyboard), name: NSNotification.Name(rawValue: EnforcementNotificationsClass.KEYBOARD_DISMISS_NOTIFICATIONS), object: nil)
        

        loginButton.onClickOfButton = {
            self.dismissKeyboard()
            
            if(self.validateCredentials()) {
                LoadingIndicator.showActivityIndicatorOnWindow()
                APIManager.sharedInstance.userAuthentication(username: self.userName.text!, password: self.password.text!.md5Encryption(), success: { (response : Any?) in
                    
                    let responseDict : NSDictionary = response as! NSDictionary
                    if(responseDict.checkForValidStatus()) {
                        LoadingIndicator.removeActivityIndicatorOnWindow()
                        let mapVerificationViewController = EnforcementCommonMethods.getViewControllerWithId(storyBoardId: "MapVerificationViewController") as! MapVerificationViewController
                        self.navigationController?.pushViewController(mapVerificationViewController, animated: true)
                        EnforcementUtils.isLoginSuccess = true
                    } else {
                        _ = SweetAlert().showAlert("Invalid Credentials", subTitle: "Username and password doesn't match", style: .warning)
                    }
                    LoadingIndicator.removeActivityIndicatorOnWindow()
                }, failure: { (error : Error?) -> (Void) in
                    LoadingIndicator.removeActivityIndicatorOnWindow()
                })
            } else {
                _ = SweetAlert().showAlert("Invalid Credentials", subTitle: "Please enter valid credentials", style: .warning)
            }
        }
        
        // Do any additional setup after loading the view.
    }

    override func viewWillLayoutSubviews() {
        userName.setUnderlined(with: UIColor.white)
        password.setUnderlined(with: UIColor.white)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(EnforcementUtils.userDefaultCredentials != nil) {
            userName.text = EnforcementUtils.userDefaultCredentials!.value(forKey: EnforcementCommonConstants.USER_NAME) as? String
            password.text = EnforcementUtils.userDefaultCredentials!.value(forKey: EnforcementCommonConstants.PASSWORD) as? String
            
            checkBox.setOn(true, animated: true)
        }
        
        checkBox.cornerRadius  = checkBox.frame.height / 2

        loginButton.layer.borderColor = UIColor.white.cgColor
        loginButton.layer.cornerRadius = loginButton.frame.height / 2
    }
    
    @IBAction func onClickOfSaveCredentials(_ sender: Any) {
        checkBox.setOn(!checkBox.isOn(), animated: true)
        self.checkANDSaveCredentials()
    }
    
    func checkANDSaveCredentials() {
        if(checkBox.isOn()) {
            let defaultCredentials : NSDictionary = [EnforcementCommonConstants.USER_NAME : userName.text!, EnforcementCommonConstants.PASSWORD : password.text!]
            EnforcementUtils.userDefaultCredentials = defaultCredentials
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validateCredentials() -> Bool {
        if((self.userName.text != nil && self.userName.text != "") && (self.password.text != nil && self.password.text != "")) {
            return true
        } else {
            return false
        }
    }
    
    //MARK:- Keyboard Notification Methods
    
    func keyboardWillShow(notification: NSNotification) {
        if(!isShowingKeyboard) {
            isShowingKeyboard = true;
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: -100)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if(isShowingKeyboard == true) {
            isShowingKeyboard = false;
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: 100)
        }
    }
    
    // MARK: - TextField Delegate methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == userName) {
            password.becomeFirstResponder()
        } else {
            password.resignFirstResponder()
        }
        return true
    }
    
    func dismissKeyboard() {
        if(userName.isFirstResponder) {
            userName.resignFirstResponder()
        } else {
            password.resignFirstResponder()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
