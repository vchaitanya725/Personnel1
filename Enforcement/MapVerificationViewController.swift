//
//  MapVerificationViewController.swift
//  Enforcement
//
//  Created by Viswa Gopisetty on 15/06/17.
//  Copyright © 2017 Viswa Gopisetty. All rights reserved.
//

import UIKit
import MapKit

class MapVerificationViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet var verificationButton: CustomButton!
    @IBOutlet var addressDisplayView: UIView!
    @IBOutlet var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet var locationMapView: MKMapView!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var addressLabelHeight: NSLayoutConstraint!
    
    var currentLocationRegion : MKCoordinateRegion? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action:#selector(handleLongPress(sender:)))
        locationMapView.addGestureRecognizer(gestureRecognizer)
        locationMapView.showsUserLocation = true

        verificationButton.onClickOfButton = {
            let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.setScanCodeViewController()
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        verificationButton.layer.cornerRadius = verificationButton.frame.height / 2

        self.getAddressStringAndSet()
    }
    
    func getAddressStringAndSet() {
        let addressString = EnforcementUtils.defaultLocation
        if(addressString != nil) {
            let heightForAddress = ("  " + addressString!).heightWithConstrainedWidth(width: addressLabel.frame.width, font: UIFont.systemFont(ofSize: 18))
            
            addressLabelHeight.constant = heightForAddress
            addressLabel.text = "  " + addressString!
        }
    }
    
    func sendClass(type : AnyClass) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(EnforcementUtils.defaultCordinate != nil) {
            var mapRegion = MKCoordinateRegion()
            mapRegion.center = EnforcementUtils.defaultCordinate!.coordinate
            mapRegion.span.latitudeDelta = 0.1
            mapRegion.span.longitudeDelta = 0.1
            locationMapView.setRegion(mapRegion, animated: true)
            
            currentLocationRegion = mapRegion
            
            let mkannotation : MKPointAnnotation = MKPointAnnotation()
            mkannotation.coordinate = EnforcementUtils.defaultCordinate!.coordinate
            mkannotation.title = "Annotation"
            locationMapView.addAnnotation(mkannotation)
            
            UIView.animate(withDuration: 0.6) {
                self.bottomViewConstraint.constant = -(self.addressDisplayView.frame.height + 30)
                self.view.layoutIfNeeded()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleLongPress(sender : UILongPressGestureRecognizer) {
        if sender.state != UIGestureRecognizerState.began { return }
        let touchLocation = sender.location(in: locationMapView)
        let locationCoordinate = locationMapView.convert(touchLocation, toCoordinateFrom: locationMapView)
        
        let annotationsToRemove = locationMapView.annotations.filter { $0 !== locationMapView.userLocation }
        locationMapView.removeAnnotations(annotationsToRemove)
        
        let mkannotation : MKPointAnnotation = MKPointAnnotation()
        mkannotation.coordinate = locationCoordinate
        mkannotation.title = "Annotation"
        locationMapView.addAnnotation(mkannotation)
        
        EnforcementCommonMethods.saveAddressForLocation(location: CLLocation(latitude: locationCoordinate.latitude, longitude: locationCoordinate.longitude), completionHandler: {
            self.getAddressStringAndSet()
        })
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if(annotation.isEqual(mapView.userLocation)) {
            return nil
        }
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "CustomAnnotation")
        if(annotationView == nil) {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "CustomAnnotation")
            if(annotation.title != nil) {
                annotationView?.image = UIImage(named: annotation.title!!)
            }
        }
        return annotationView
    }
    
    @IBAction func onClickOfCurrentRegion(_ sender: Any) {
        if(currentLocationRegion != nil) {
            locationMapView.setRegion(currentLocationRegion!, animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
