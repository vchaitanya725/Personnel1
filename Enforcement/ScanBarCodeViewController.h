//
//  BarCodeViewController.h
//  Verit
//
//  Created by Viswa Gopisetty on 20/05/17.
//  Copyright © 2017 Viswa Gopisetty. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZXingObjC/ZXingObjC.h>

@protocol DidFinishBarCodeScan <NSObject>
-(void)didFinishScanningBarCode:(NSString *)code;
@end

@interface ScanBarCodeViewController : UIViewController <ZXCaptureDelegate>{
    
}
@property id<DidFinishBarCodeScan> delegate;
@property (nonatomic,strong) NSString *scannedCode;

-(void)startScanning;
-(void)stopScanning;

@end
